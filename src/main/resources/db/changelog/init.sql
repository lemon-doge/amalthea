insert into person(id, email)
values (1000001, 'person1@gmail.com'),
       (1000002, 'person2@gmail.com'),
       (1000003, 'person3@gmail.com'),
       (1000004, 'person4@gmail.com'),
       (1000005, 'person5@gmail.com');

insert into currency(id, code, name)
values (1000006, 'USD', 'Доллар'),
       (1000007, 'EUR', 'Евро'),
       (1000008, 'RUB', 'Рубль');

