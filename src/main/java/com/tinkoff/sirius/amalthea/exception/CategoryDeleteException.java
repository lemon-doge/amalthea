package com.tinkoff.sirius.amalthea.exception;

public class CategoryDeleteException extends Exception {
    public CategoryDeleteException() {
    }

    public CategoryDeleteException(String message) {
        super(message);
    }
}
