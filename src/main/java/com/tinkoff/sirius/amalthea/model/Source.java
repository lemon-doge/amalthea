package com.tinkoff.sirius.amalthea.model;

public enum Source {
    SYSTEM, CUSTOM
}
